### dapido ###

Sample Web Application for booking riders similar to Rapido(http://rapido.bike/) using Nodejs and Socket.io

**Functionality:**
Simulates riders on map and allows users to book them by clicking on them.
Riders booked will be available online after 10sec timeout.

**Requirements:**

- Node.js
Download link - https://nodejs.org/en/download/
Alternatively, you can install using package managers - https://nodejs.org/en/download/package-manager/

- Socket.io
npm install --save socket.io
You can quickly get started by looking at this sample chat application - http://socket.io/get-started/chat/

- Express Framework
npm install --save express@4.10.2

**How to improve?**
You can use Snap to Raods method from Google Maps Roads API
Add Firebase to complete the App (https://moquet.net/blog/realtime-geolocation-tracking-firebase/)