/*
* Node Server Code
*
* Users can view available riders on Map;
* They can book a rider by clicking on the icons;
*
* Simulation: 
* Riders will be moving in random directions within the boundaries.
* When a rider is booked, his state is changed to off and will be made available after 10 seconds at some new random location.
*
*/

//Config
_riders_count = 50
_rider_inactive_time = 10000 //milliseconds

// Smaller boundary
//_lat_limits = {"min": 17.295902, "max": 17.506228}
//_lng_limits = {"min": 78.377646, "max": 78.604239}

_lat_limits = {"min": 17.197351, "max": 17.604367}
_lng_limits = {"min": 78.266066, "max": 78.686351}

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var utils = require('./utils');
var clients = [];

app.get('/', function(req, res){
  res.sendFile('index.html', { root : __dirname});
});

app.get('/bike.png', function(req, res){
  res.sendFile('/img/bike.png', { root : __dirname});
});

_riders = utils.generateRandomRiders(_riders_count);
console.log(_riders_count + " riders have been created!");

_riders_on = _riders;
_riders_off = {};

// Event is fired every time a new client connects:
io.on('connection', function(socket){

	// When socket disconnects, remove it from the list and log it:
	socket.on('disconnect', function(){
	  var index = clients.indexOf(socket);
        if (index != -1) {
            clients.splice(index, 1);
            console.info('Client has left (id=' + socket.id + ').');
       }
	});

  socket.on('rider_select',function(rider_t){
        var rider = _riders_on[rider_t["name"]];
        delete _riders_on[rider["name"]];
        _riders_off[rider["name"]] = rider;

        console.log(rider["name"] + " rider is booked by client with socket id - "+socket.id);

        io.emit('remove_rider', rider);

        setTimeout(function(){
            console.log(rider["name"]);
            var rider_t = _riders_off[rider["name"]];
            rider_t["location"] = utils.getRandomPosition();
            _riders_on[rider_t["name"]] = rider_t;
            delete _riders_off[rider_t["name"]];

            console.log(rider_t["name"] + " rider is back online");

        },_rider_inactive_time);
  });

	console.info('New client connected (id=' + socket.id + ').');
  clients.push(socket);
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

// To simulate rider location changes
setInterval(function() {
  for(rider_id in _riders_on){
    _riders_on[rider_id]["location"] = utils.moveRiderByDelta(_riders_on[rider_id]["location"]);
  }
  io.emit('riders', utils.getValuesArray(_riders_on));
}, 1000);